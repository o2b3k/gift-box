<h2>Server Requirements</h2><br>
Composer<br>
MySQL 5.6<br>
PHP >= 7.0.0<br>
OpenSSL PHP Extension<br>
PDO PHP Extension<br>
Mbstring PHP Extension<br>
Tokenizer PHP Extension<br>
XML PHP Extension<br>

<h2>Installation</h2>
Download this repository<br>
Rename `.env.example` to `.env` and fill the options.<br>
Run the following commands:
```
composer install
php artisan key:generate
php artisan migrate
php artisan serve

```

<h2>Custom command</h2>
``` 
php artisan payment:send
```