<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $comment;

    /**
     * Create a new message instance.
     *
     * @param $subject
     * @param $comment
     */
    public function __construct($subject, $comment)
    {
        $this->subject = $subject;
        $this->comment = $comment;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.mail');
    }
}
