<?php

namespace App\Http\Controllers;

use App\History;
use App\Mail\NotificationMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class GiftController extends Controller
{
    public function getGift()
    {
        $type = "";
        $gifts = array(random_int(100, 999), random_int(10, 100), array("Book", "Pencil", "Phone"));
        $result = array_rand($gifts, 1);
        switch ($result) {
            case 0:
                $type = "Денежный сумму";
                $this->saveHistory($type, $gifts[$result]);
                break;
            case 1:
                $type = "Бонусные баллы";
                $this->saveHistory($type, $gifts[$result]);
                $this->sendMail($gifts[$result]);
                break;
        }
        if ($result == 2) {
            $type = "Предмет";
            $num = array_rand($gifts[2], 1);
            $this->saveHistory($type, $gifts[2][$num]);
            return response()->json(['result' => $gifts[2][$num], "type" => $type]);
        } else {
            return response()->json(['result' => $gifts[$result], "type" => $type]);
        }

    }

    public function exchange()
    {
        $money_history = History::where('user_id', auth()->user()->getAuthIdentifier())
            ->where('type', 'Денежный сумму')
            ->where('status', true)
            ->get();
        $money = 0;
        foreach ($money_history as $item) {
            $money = $money + $item->sum;
        }
        return view('exchange.index', ['money' => $money, 'histories' => $money_history]);
    }

    public function exchangeMoney(Request $request)
    {
        $history = History::where('id', $request->input('money_id'))->first();
        $new_sum = (int)$history->sum * 1.5;
        $history->status = false;
        $history->save();
        $this->saveHistory("Обмен (Денги -> Балл)", $new_sum);
        return redirect()->route('exchange.index');
    }

    public function saveHistory($type, $sum)
    {
        $history = new History();
        $history->type = $type;
        $history->sum = $sum;
        $history->status = true;
        $history->user_id = auth()->user()->getAuthIdentifier();
        $history->save();
    }

    public function testBankApi(Request $request)
    {
        $money = $request->post('money');
        if ($money){
            return response()->json(['success' => true], 200);
        }else{
            return response()->json(['success' => false], 500);
        }
    }

    public function sendMail($sum)
    {
        $email = Auth::user()->email;
        $subject = "Приз";
        $comment = $sum;
        Mail::to($email)->send(new NotificationMail($subject, $comment));
    }
}
