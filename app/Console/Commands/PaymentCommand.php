<?php

namespace App\Console\Commands;

use App\History;
use Illuminate\Console\Command;

class PaymentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Payment send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $money_history = History::where('type', 'Денежный сумму')
            ->where('status', true)
            ->get();
        $client = new \GuzzleHttp\Client();
        $url = "http://127.0.0.1:8000/api/bank";
        foreach ($money_history as $money) {
            $response = $client->request('POST', $url,[
                'form_params' => [
                    'money' => (int)$money->sum,
                ]
            ]);
            $status = $response->getStatusCode();
            if ($status == 200) {
                echo "Payment successfully send\n";
            } else {
                echo "Error";
            }
        }
    }
}
