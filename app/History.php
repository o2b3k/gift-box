<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'histories';

    protected $fillable = ['type', 'sum', 'user_id', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
