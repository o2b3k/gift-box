@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Главная</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="jumbotron">
                            <h1 class="display-4">Gift, Box!</h1>
                            <p class="lead">В сервис случайностей😇
                                В этом случайно счастливом сервисе ты сможешь бесплатно выиграть призы.
                                Первая случайность сумма.
                                Вторая случайность
                                бонусные баллы.
                                Третя случайность предмет из списка.
                                Это твой шанс!
                            </p>
                            <hr class="my-4">
                            <p>Для выигрыша приза нужно нажать на кнопку снизу.</p>
                            <p class="lead">
                            <center><h1 id="timer" style="display: none;">3</h1></center>
                            <center><a class="btn btn-success btn-lg" href="#"
                                       role="button" onclick="startTime()">ПОГНАЛИ</a></center>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>

        let myVar = 0;
        let t = 3;

        function startTime() {
            myVar = setInterval(myTimer, 1000);
            document.getElementById("timer").style.display = "block";
        }

        function myTimer() {
            document.getElementById("timer").innerHTML = t;
            t -= 1;
            if (t === 0) {
                clearInterval(myVar);
                $.ajax({
                    type: 'GET',
                    url: "{{ route('get.gift') }}",
                    success: function (data) {
                        document.getElementById("timer").style.fontSize = "50px";
                        let type = data['type'];
                        switch (type) {
                            case "Денежный сумму":
                                document.getElementById("timer").innerHTML = "Ваш приз " + data['result'] + "$ "
                                    + data['type'] + "\n Поздравляю!";
                                break;
                            case "Бонусные баллы":
                                document.getElementById("timer").innerHTML = "Ваш приз " + data['result'] + " "
                                    + data['type'] + "\n Поздравляю!";
                                break;
                            case "Предмет":
                                document.getElementById("timer").innerHTML = "Ваш приз " + data['result'] + " "
                                    + data['type'] + "\n Поздравляю!";
                                break;
                            default:
                                document.getElementById("timer").innerHTML = "Ошибка Опс";
                        }
                    },
                });
                t = 3;
            }
        }
    </script>
@stop
