@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Обмен</div>

                    <div class="card-body">
                        <h4>У вас есть {{ $money }} вы можете обменьят по курсу</h4>
                            <h4>1 денежный сумму - 1.5 бонусный балл</h4>
                        <hr>
                        <form class="form-inline" action={{ route('exchange.money') }} method="post">
                            @csrf
                            <div class="form-group">
                                <select name="money_id" id="" class="form-control">
                                    <option value="0">Выберите сумму</option>
                                    @foreach($histories as $money)
                                        <option value="{{ $money->id }}">{{ $money->sum }} денежный сумму</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Обменять</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
